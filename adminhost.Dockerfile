﻿FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

# 安装libgdiplus库，用于Excel导出
RUN apt-get update && apt-get install -y libgdiplus

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY ["src/web/Admin.Host/Admin.Host.csproj", "src/web/Admin.Host/"]
COPY ["src/web/Admin.Web.Core/Admin.Web.Core.csproj", "src/web/Admin.Web.Core/"]
COPY ["src/application/Admin.Application/Admin.Application.csproj", "src/application/Admin.Application/"]
COPY ["src/core/Magicodes.Admin.Core/Magicodes.Admin.Core.csproj", "src/core/Magicodes.Admin.Core/"]
COPY ["src/data/Magicodes.Admin.EntityFrameworkCore/Magicodes.Admin.EntityFrameworkCore.csproj", "src/data/Magicodes.Admin.EntityFrameworkCore/"]
COPY ["src/core/Magicodes.Admin.Core.Custom/Magicodes.Admin.Core.Custom.csproj", "src/core/Magicodes.Admin.Core.Custom/"]
COPY ["src/application/Admin.Application.Custom/Admin.Application.Custom.csproj", "src/application/Admin.Application.Custom/"]
RUN dotnet restore "src/web/Admin.Host/Admin.Host.csproj"
COPY . .
WORKDIR "/src/src/web/Admin.Host"
RUN dotnet build "Admin.Host.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Admin.Host.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Magicodes.Admin.Web.Host.dll"]